public class Student {
	
	private String nip;
	private String name;
	private double salary;
		
	public Student (String nip, String name, double salary) {
		this.nip = nip;
		this.name = name;
		this.salary = salary;
		
	}

	public String getnip() {
		return nip;
	}

	public void setnip(String nip) {
		this.nip = nip;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
}
